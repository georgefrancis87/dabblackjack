//
//  ViewController.h
//  DABBlackjack
//
//  Created by George Francis on 24/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Game.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController <GameDelegate>

@property (strong, nonatomic) IBOutlet UILabel *gameStatusLabel;
@property (strong, nonatomic) IBOutlet UIView *cardViewContainer;


@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *dealerPlaceholders;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *dealerCardsImageViews;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *playerCardsImageViews;

@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

- (IBAction)hitButtonPressed:(id)sender;
- (IBAction)checkButtonPressed:(id)sender;
- (IBAction)dealButtonPressed:(id)sender;

-(void)newGame;


@end
