//
//  Player+CoreDataProperties.m
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Player+CoreDataProperties.h"

@implementation Player (CoreDataProperties)

@dynamic handValue;
@dynamic isDealer;
@dynamic isStand;
@dynamic cards;
@dynamic game;

@end
