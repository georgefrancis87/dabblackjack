//
//  ViewController.m
//  DABBlackjack
//
//  Created by George Francis on 24/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import "ViewController.h"


@interface ViewController ()

@end

@implementation ViewController
{
    Game *game;
    AppDelegate *delegate;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    _managedObjectContext = [delegate managedObjectContext];

    Game *savedGame = [Game gameInContext:_managedObjectContext];
    
    if (savedGame) {
        game = savedGame;
        game.delegate = self;
        [self play];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.cardViewContainer.layer.cornerRadius = 138;
    self.cardViewContainer.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cardViewContainer.layer.borderWidth = 5;
}

-(void)newGame
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Game" inManagedObjectContext:self.managedObjectContext];
    game = (Game*)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];
    game.delegate = self;
    [game createDealerHand];
}

-(void)emptyImageViews
{
    for (UIImageView *imgView in self.dealerCardsImageViews) {
        imgView.image = nil;
    }
    
    for (UIImageView *imgView in self.playerCardsImageViews) {
        imgView.image = nil;
    }
}

-(void)play
{
    for (Player *player in game.players) {

        if (player.isDealer.boolValue) {
        
            for (Card *card in player.cards) {
                UIImageView *cardImageView = [self.dealerCardsImageViews objectAtIndex:[player.cards indexOfObject:card]];
                cardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@",card.suit,card.rank]];
            }
        } else {
            for (Card *card in player.cards) {
                UIImageView *cardImageView = [self.playerCardsImageViews objectAtIndex:[player.cards indexOfObject:card]];
                cardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@",card.suit,card.rank]];
            }
            [self checkPlayerHand];
        }
    }
}

-(void)checkPlayerHand
{
    Player *player = [game player];
    
    if (player.isStand.boolValue) {
        [self playDealerHand];
        
    } else if ([player hasPlayerBust]) {
        self.gameStatusLabel.text = @"YOU BUST DEALER WINS";
    }
}

-(void)playDealerHand
{
    Player *dealer = [game dealer];
    
    if ([dealer hasPlayerBust]) {
        self.gameStatusLabel.text = @"DEALER BUSTS";
        return;
    }
    
    if (dealer.handValue < [NSNumber numberWithInt:14]) {
        [game addCardToPlayer:NO];
    } else {
        [self checkWinner];
    }
}

-(void)checkWinner
{
    Player *dealer = [game dealer];
    Player *player = [game player];

    if (dealer.handValue > player.handValue) {
        self.gameStatusLabel.text = @"DEALER WINS";
    }
    else {
        self.gameStatusLabel.text = @"YOU WIN";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)hitButtonPressed:(id)sender
{
    [game addCardToPlayer:YES];
}

- (IBAction)checkButtonPressed:(id)sender
{
    Player *player = [game player];
    player.isStand = @YES;
    [self playDealerHand];
}

- (IBAction)dealButtonPressed:(id)sender
{
    if (game != nil) {
        [_managedObjectContext deleteObject:game];
    }

    self.gameStatusLabel.text = @"BLACKJACK";
    [self emptyImageViews];
    [self newGame];
}

@end
