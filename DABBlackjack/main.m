//
//  main.m
//  DABBlackjack
//
//  Created by George Francis on 24/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
