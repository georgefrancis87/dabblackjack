//
//  Player.h
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Card, Game;

NS_ASSUME_NONNULL_BEGIN

@interface Player : NSManagedObject

-(void)createHandValue;
-(BOOL)hasPlayerBust;

@end

NS_ASSUME_NONNULL_END

#import "Player+CoreDataProperties.h"
