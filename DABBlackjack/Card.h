//
//  Card.h
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Player;

NS_ASSUME_NONNULL_BEGIN

@interface Card : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Card+CoreDataProperties.h"
