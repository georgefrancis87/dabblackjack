//
//  Player+CoreDataProperties.h
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Player.h"

NS_ASSUME_NONNULL_BEGIN

@interface Player (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *handValue;
@property (nullable, nonatomic, retain) NSNumber *isDealer;
@property (nullable, nonatomic, retain) NSNumber *isStand;
@property (nullable, nonatomic, retain) NSOrderedSet<Card *> *cards;
@property (nullable, nonatomic, retain) Game *game;

@end

@interface Player (CoreDataGeneratedAccessors)

- (void)insertObject:(Card *)value inCardsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromCardsAtIndex:(NSUInteger)idx;
- (void)insertCards:(NSArray<Card *> *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeCardsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInCardsAtIndex:(NSUInteger)idx withObject:(Card *)value;
- (void)replaceCardsAtIndexes:(NSIndexSet *)indexes withCards:(NSArray<Card *> *)values;
- (void)addCardsObject:(Card *)value;
- (void)removeCardsObject:(Card *)value;
- (void)addCards:(NSOrderedSet<Card *> *)values;
- (void)removeCards:(NSOrderedSet<Card *> *)values;

@end

NS_ASSUME_NONNULL_END
