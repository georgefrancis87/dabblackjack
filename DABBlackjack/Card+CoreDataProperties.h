//
//  Card+CoreDataProperties.h
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Card.h"

NS_ASSUME_NONNULL_BEGIN

@interface Card (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *rank;
@property (nullable, nonatomic, retain) NSString *suit;
@property (nullable, nonatomic, retain) NSNumber *value;
@property (nullable, nonatomic, retain) Player *player;

@end

NS_ASSUME_NONNULL_END
