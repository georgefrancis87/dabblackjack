//
//  Player.m
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import "Player.h"
#import "Card.h"
#import "Game.h"

@implementation Player

-(void)createHandValue
{
    NSNumber *playerScore;
    
    for (Card *card in self.cards) {
        playerScore = @([playerScore integerValue] + [card.value integerValue]);
    }
    
    self.handValue = playerScore;
    [self.managedObjectContext save:nil];
}

-(BOOL)hasPlayerBust
{
    if (self.handValue > [NSNumber numberWithInt:21]) {
        
        for (Card *card in self.cards) {
            
            if ([card.value isEqual:[NSNumber numberWithInt:11]]) {
                
                card.value = [NSNumber numberWithInt:1];
                [self createHandValue];
            }
        }
    }
    
    if (self.handValue > [NSNumber numberWithInt:21]) {
        return YES;
    }
    
    return NO;
}

@end
