//
//  Game.m
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import "Game.h"
#import "Player.h"
#import "Card.h"

#define SuitArray [NSArray arrayWithObjects:@"s",@"c",@"d",@"h",nil]
#define RankArray [NSArray arrayWithObjects:@"a",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"j",@"q",@"k",nil]

@implementation Game

@synthesize delegate = _delegate;

-(void)createDealerHand
{
    Player *player = [self createPlayer];
    [self createCardForPlayer:player];
    [self createCardForPlayer:player];
    player.isDealer = @YES;
    [player createHandValue];
    [self addPlayerToGame:player];
    [self createPlayerHand];
}

-(void)addPlayerToGame:(Player*)player
{
    NSMutableSet *set = [self.players mutableCopy];
    [set addObject:player];
    self.players = [NSSet setWithSet:set];
}

-(void)createPlayerHand
{
    Player *player = [self createPlayer];
    [self createCardForPlayer:player];
    [self createCardForPlayer:player];
    player.isDealer = @NO;
    [player createHandValue];
    [self addPlayerToGame:player];
    [self.managedObjectContext save:nil];
    [self.delegate play];
}

-(Player*)player
{
    for (Player *player in self.players) {
        if (!player.isDealer.boolValue) {
            return player;
        }
    }
    return nil;
}

-(Player*)dealer
{
    for (Player *player in self.players) {
        if (player.isDealer.boolValue) {
            return player;
        }
    }
    return nil;
}

-(void)createCardForPlayer:(Player*)player
{
    Card *card = [self createCard];
    NSMutableOrderedSet *set = [player.cards mutableCopy];
    [set addObject:card];
    player.cards = [NSOrderedSet orderedSetWithOrderedSet:set];
}

-(Player*)createPlayer
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Player" inManagedObjectContext:self.managedObjectContext];
    Player *player = (Player*)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];
    
    return player;
}

-(Card*)createCard
{
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"Card" inManagedObjectContext:self.managedObjectContext];
    Card *newCard = (Card*)[[NSManagedObject alloc]initWithEntity:entityDescription insertIntoManagedObjectContext:self.managedObjectContext];

    NSString *suit = [SuitArray objectAtIndex:arc4random()%[SuitArray count]];
    newCard.suit = suit;
    newCard.rank = [self getRank];
    newCard.value = [NSNumber numberWithInt:[self getValue:newCard.rank]];

    return newCard;
}

-(void)addCardToPlayer:(BOOL)isPlayer
{
    for (Player *player in self.players) {
        
        if (player.isDealer.boolValue == !isPlayer) {
            
            [self createCardForPlayer:player];
            [player createHandValue];
        }
    }
    [self.delegate play];
}

-(NSNumber*)playerScore:(BOOL)isPlayer
{
    for (Player *player in self.players) {
        if (player.isDealer.boolValue == !isPlayer) {
            return player.handValue;
        }
    }
    return nil;
}

-(NSString*)getRank
{
    NSString *rank = [RankArray objectAtIndex:arc4random()%[RankArray count]];
    return rank;
}

-(int)getValue:(NSString*)test
{
    if ([test isEqualToString:@"a"]) {
        return 11;
    } else if ([test isEqualToString:@"j"]) {
        return 10;
        
    } else if ([test isEqualToString:@"q"]) {
        return 10;
        
    }else if ([test isEqualToString:@"k"]){
        return 10;
    }
    return [test intValue];
}

+ (Game*)gameInContext:(NSManagedObjectContext*)context
{
    NSFetchRequest *fetchBagRequest = [NSFetchRequest fetchRequestWithEntityName:NSStringFromClass([Game class])];
    NSArray *fetchedBagArray = [context executeFetchRequest:fetchBagRequest error:nil];
    
    Game *game = fetchedBagArray.lastObject;
    
    return game;
}


@end
