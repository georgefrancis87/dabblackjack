//
//  Game.h
//  DABBlackjack
//
//  Created by George Francis on 25/05/2016.
//  Copyright © 2016 George Francis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@protocol GameDelegate <NSObject>

-(void)play;

@end

@class Player;

NS_ASSUME_NONNULL_BEGIN

@interface Game : NSManagedObject

-(NSNumber*)playerScore:(BOOL)isPlayer;
-(void)createDealerHand;
-(void)addCardToPlayer:(BOOL)isPlayer;

-(Player*)dealer;
-(Player*)player;

+ (Game*)gameInContext:(NSManagedObjectContext*)context;

@property (nonatomic, assign) id<GameDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

#import "Game+CoreDataProperties.h"
